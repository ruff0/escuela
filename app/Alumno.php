<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $table = 'alumnos';

    protected $fillable = ['nombre', 'ap_paterno', 'ap_materno', 'activo'];

    public $timestamps = false;
    // public function materias()
    // {
    //   return $this->hasMany('App\Materia');
    // }

    public function calificaciones()
    {
      return $this->hasMany('App\Calificacion');
    }
}
