<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
  protected $table = 'calificaciones';

  protected $fillable = ['id_materias', 'id_alumnos', 'calificacion'];

  public $rules = [
      'id_materias' => 'required',
      'id_alumnos' => 'required',
      'calififcacion' => 'required',
  ];

  public $timestamps = false;

}
