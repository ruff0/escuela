<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $collection = 'contact';

    protected $fillable = ['firstName', 'surName', 'picture', 'phones', 'emails'];

    public function phones()
    {
      return $this->hasMany('App\Phone');
    }

    public function emails()
    {
      return $this->hasMany('App\Email');
    }
}
