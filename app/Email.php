<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
  protected $collection = 'email';

  protected $fillable = ['contact_id', 'address'];

  public $rules = [
      'contact_id' => 'required',
      'address' => 'required',
  ];
}
