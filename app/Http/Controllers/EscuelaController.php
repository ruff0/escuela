<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use App\Materia;
use App\Calificacion;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class EscuelaController extends Controller
{
    public function index()
    {

        $calificaciones = Calificacion::all();
        $data = [];
        foreach ($calificaciones as $calificacion => $c) {
          $alumno = Alumno::find($c->id_alumnos);
          $materia = Materia::find($c->id_materias);
          $data[] = array(
            "id_alumno"     => $alumno->id,
            "nombre"        => $alumno->nombre.' '.$alumno->ap_paterno.' '.$alumno->ap_materno,
            "materia"       => $materia->nombre,
            "calificacion"  => $c->calificacion);
        }
        return ResponseBuilder::success($data);

    }

    public function create(Request $request)
    {
        Alumno::create($request->all());
        return Alumno::orderBy('id', 'desc')->first();
    }

    public function show($id)
    {
         $alumno = Alumno::findOrFail($id);
         if($alumno !== false)
         {
           $calificaciones = Calificacion::all()->where('id_alumnos','=',$id);
           $suma = 0;
           $promedio = 0;
	   $cantidad = count($calificaciones);
	   if($cantidad > 0){
             foreach($calificaciones as $calificacion => $c){
                $suma = $suma + $c->calificacion;
             }  
             $promedio = ($suma / $cantidad);
	   }
         }
         $data = ["calificaciones" => $calificaciones, "promedio"=> $promedio];
         return ResponseBuilder::success($data);
    }

    public function addNote(Request $request, $id)
    { 
      if($request->calificacion > 10 || $request->calificacion < 0) return ResponseBuilder::error(210);
      $materia = Materia::find($request->materia);
      if(!isset($materia)) return ResponseBuilder::error(200);
      if(!null == Calificacion::where('id_alumnos','=',$id)->where('id_materias','=',$materia->id)->first()) 
	return ResponseBuilder::error(220, null,  $data='ya fue calificado anteriormente');
      $calificacion = new Calificacion;
      $calificacion->calificacion = $request->calificacion;
      $calificacion->id_materias = $materia->id;
      $calificacion->id_alumnos = $id;
      $calificacion->save();
      return ResponseBuilder::success("calificacion registrada");
    }

    public function updateNote(Request $request, $id)
    {
      $materia = Materia::find($request->json('materia'));
      if(!isset($materia)) return ResponseBuilder::error(200);
      $calificacion = Calificacion::where('id_alumnos','=',$id)
        ->where('id_materias','=',$materia->id)
        ->update(['calificacion' => $request->calificacion]);
      return ResponseBuilder::success("calificacion actualizada");
    }


    public function destroyNote(Request $request, $id)
    {
      $materia = Materia::find($request->json('materia'));
      if(!isset($materia)) return ResponseBuilder::error(200);
      $calificacion = Calificacion::first()->where('id_alumnos','=',$id)
        ->where('id_materias','=',$materia);
      if(!$calificacion) return ResponseBuilder::error(230, null, $data='no existe');
      $calificacion->delete();
      return ResponseBuilder::success("calificacion eliminada");
    }
}













