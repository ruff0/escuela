<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
  protected $table = 'materias';

  protected $fillable = ['nombre', 'activo'];

  public $rules = [
      'nombre' => 'required',
      'activo' => 'required',
  ];

  public $timestamps = false;

}
