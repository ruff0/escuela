<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
  protected $collection = 'phone';

  protected $fillable = ['contact_id', 'number'];

  public $rules = [
      'contact_id' => 'required',
      'number' => 'required',
  ];
}
