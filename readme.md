## Para instalar.

git clone https://ruff0@bitbucket.org/ruff0/escuela.git
cd escuela
composer install -vvv && composer update -vvv

## Ejecuta la migracion y siembra los datos para un usuario.

./artisan migrate --seed

## Configura el archivo .env y después inicia el servidor.

./artisan serve

## POST localhost:8000/api/login
{
  "email": "just@test.mail",
  "password": "password"
}
# obtendras un access_token de tipo bearer que es necesario
enviar en el header para acceder a las siguientes servicios.


# para obtener la lista completa de las rutas:
./artisan route:list

## GET localhost:8000/api/alumnos
obtiene las calificaciones de los alumnos.

## POST localhost:8000/api/alumnos  
# crea un nuevo alumno
{
  "nombre": "john",
  "ap_paterno": "mc",
  "ap_materno": "wallace"
}

## POST localhost:8000/api/alumnos/id_alumno  
# registra calificacion
{
  "materia": "1",
  "calificacion": "9.5",
}

## PUT  localhost:8000/api/alumnos/id_alumno  
# actualiza calificacion
{
  "materia": "1",
  "calificacion": "10",
}

## PUT  localhost:8000/api/alumnos/id_alumno  
# elimina calificacion
{
  "materia": "1",
}

